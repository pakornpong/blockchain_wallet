import  Vue from 'vue'
import VueRouter from 'vue-router'
import Main from "../views/Main";
import Players from "../views/Players";
import Token from "../views/Token";
import test from "../views/test";
Vue.use(VueRouter);
const routes = [
    {
        path: '/',
        name: 'Main',
        component: Main
    },
    {
        path: '/players',
        name: 'Players',
        component: Players
    },
    {
        path: '/token',
        name: 'Token',
        component: Token
    },
    {
        path: '/test',
        name: 'test',
        component: test
    },
];

const router = new VueRouter({
    scrollBehavior() {
        return { x: 0, y: 0 };
    },
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

export default router
